Name:           libass
Version:        0.17.3
Release:        2
Summary:        Portable subtitle renderer for the ASS/SSA subtitle format
License:        ISC
URL:            https://github.com/libass

Patch0:         backport-fontconfig-fix-minor-memory-leak.patch

Source0:        https://github.com/libass/libass/releases/download/%{version}/libass-%{version}.tar.xz
BuildRequires:  gcc nasm pkgconfig(fontconfig) >= 2.10.92 pkgconfig(freetype2) >= 9.10.3
BuildRequires:  pkgconfig(fribidi) >= 0.19.0 pkgconfig(harfbuzz) >= 0.9.5 pkgconfig(libpng) >= 1.2.0

%description
libass is a portable subtitle renderer for the ASS/SSA (Advanced Substation Alpha/Substation Alpha) 
subtitle format. It is mostly compatible with VSFilter.

%package        devel
Summary:        Development files for libass
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
The package contains libraries and header files for developing of libass applications.

%package_help

%prep
%autosetup -p1

%build
%configure
%make_build

%install
%make_install
%delete_la_and_a

%check
make check

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%license COPYING
%{_libdir}/*.so.9*

%files devel
%defattr(-,root,root)
%{_includedir}/ass
%{_libdir}/*.so
%{_libdir}/pkgconfig/libass.pc

%files help
%defattr(-,root,root)
%doc Changelog

%changelog
* Wed Sep 25 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 0.17.3-2
- fontconfig: fix minor memory leak

* Wed Jul 03 2024 wangkai <13474090681@163.com> - 0.17.3-1
- Update to 0.17.3
- This release brings optimized assembly routines for aarch64,
  as well as numerous individual improvements and fixes.
- Fix rendering of \h in certain cases
- Fix a minor memory leak in the CoreText and DirectWrite font provider
- make check now runs if assembly is enabledcheckasm
- Fix configure generated with slibtool-provided autoconf macros
- Fix for shared-only buildsmake check
- Add new and API functionsass_mallocass_free
- Improve handling of HarfBuzz-related failures

* Fri Apr 28 2023 liyanan <thistleslyn@163.com> - 0.17.1-1
- Update to 0.17.1

* Thu Jul 29 2021 houyingchao <houyingchao@huawei.com> - 0.15.0-2
- Fix CVE-2020-36430

* Fri Feb 5 2021 zhanghua <zhanghua40@huawei.com> - 0.15.0-1
- update to 0.15.0 to fix CVE-2020-26682

* Wed Dec 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.14.0-3
- Package init
